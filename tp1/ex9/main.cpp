#include <time.h>//to estimate the runing time
#include <string>
#include <vector>
#include <map>
#include <utility>
#include <unordered_set>
#include <set>
#include <ostream>
#include <sstream>
#include <algorithm>    // std::random_shuffle

#include "adjarray.hpp"
#include <math.h>       /* sqrt */

int count_triangles(const AdjArray &adjarr) {

	std::vector<std::pair<unsigned int, std::set<unsigned int>>> ord_adjarray(adjarr.n, {0, {0}});

	int n_triang = 0;

	int ind = 0;
	for (auto kv : adjarr.adjarr) {
		std::set<unsigned int> s;
    	for(auto val:kv.second) { s.insert(val); }

		ord_adjarray[ind] = std::pair<unsigned int, std::set<unsigned int>>(kv.first, s);
		ind++;
	}

	// std::cout << "\nOrdered Adj array" << '\n';
	// for (size_t ind=0; ind<ord_adjarray.size(); ind++) {
	// 	std::cout << ord_adjarray[ind].first << "\t";
	// 	for (auto v : ord_adjarray[ind].second)
	// 		std::cout << v << "  ";
	// 	std::cout << '\n';
	// }
	// std::cout << '\n';

	// sort adjacency array
	std::sort(ord_adjarray.begin(), ord_adjarray.end(), [](std::pair<unsigned int, std::set<unsigned int>> &a, std::pair<unsigned int, std::set<unsigned int>> &b){
		return a.second.size() < b.second.size();
	});

	// std::cout << "\nOrdered Adj array" << '\n';
	// for (size_t ind=0; ind<ord_adjarray.size(); ind++) {
	// 	std::cout << ord_adjarray[ind].first << "\t";
	// 	for (auto v : ord_adjarray[ind].second)
	// 		std::cout << v << "  ";
	// 	std::cout << '\n';
	// }
	// std::cout << '\n';

	std::vector<int> vertex_to_ind(ord_adjarray.size(), 0);
	for (size_t ind=0; ind<ord_adjarray.size(); ind++) {
		vertex_to_ind[ord_adjarray[ind].first] = ind;
	}

	for (size_t ind=0; ind<ord_adjarray.size(); ind++) {
		unsigned int vert1 = ord_adjarray[ind].first;
		// std::cout << "-- " << vert1 << '\n';
		for (unsigned int vert2 : ord_adjarray[ind].second) {
			// std::cout << "   -- " << vert2 << '\n';
			// get common neighboor intersection

			std::set<unsigned int> common_neigh;

			std::set_intersection (ord_adjarray[vertex_to_ind[vert1]].second.begin(), ord_adjarray[vertex_to_ind[vert1]].second.end(), 
									ord_adjarray[vertex_to_ind[vert2]].second.begin(), ord_adjarray[vertex_to_ind[vert2]].second.end(), std::inserter(common_neigh,common_neigh.begin()));

			for (unsigned int vert3 : common_neigh)
			{
				// std::cout << "      --- " << vert3 << '\n';
				if ((vert1 < vert2) && (vert2 < vert3)) {
					// std::cout << "##" << vert1 << "   " << vert2 << "   " << vert3 << '\n';
					n_triang++;
				}
			}

		}
	}

	return n_triang;
}

int main(int argc,char** argv){
	time_t t1,t2;

	t1=time(NULL);

	if (argc!=2) {
		std::cout << "Expected input:" << '\n';
		std::cout << "   - edgelist file path" << '\n';
		return 0;
	}

	// Load graph data
	std::string filePath = std::string(argv[1]);
	AdjArray adjarr = adjarray_from_txt(filePath, false);

	std::cout << "n = " << adjarr.n << '\n';
	std::cout << "m = " << adjarr.m << '\n';
	// std::cout << "el length = " << adjarr.adjarr.size() << '\n';
	
	int ntriangles = count_triangles(adjarr);

	std::cout << "Number of triangles:  " << ntriangles << '\n';

	// std::sort(mymap.begin(), mymap.end(), [](std::pair<std::string, int> &a, std::pair<std::string, int> &b)
	// 	{ 
	// 		return a.first < b.first;
	// 	});


	// for (auto kv : mymap)
	// 	std::cout << kv.first << "\t" << kv.second << '\n';


	// // write result
	// std::ofstream output_file(std::string(argv[2]), std::ios::ate);

	// int ind = 0;
	// for (auto it_label = vertex_to_label.begin(); it_label!=vertex_to_label.end(); ind++, it_label++) {
	// 	if (*it_label != 0) {
	// 		// output_file << ind << std::endl;
	// 		output_file << std::to_string(ind) + "," + std::to_string(*it_label) << std::endl;
	// 	}
	// }
	// output_file.close();

	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}