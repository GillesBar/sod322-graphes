#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "BFS.h"


int main(int argc,char** argv){

	adjacency *g;
	time_t t1,t2;
    unsigned long start;

	t1=time(NULL);

	printf("Reading edges from file %s\n",argv[1]);
	g=readadjacency(argv[1]);
	
	list *out = BFS(g,1);
	//for (unsigned int i = 0 ; i < out->size ; i++){
	//	printf("%lu ",out->values[i]);
	//}
    start = out->values[out->size-1];
    list *out2 = BFS(g,start);

    printf("Diameter lower bound: %lu",out2->marked[out2->size-1]);

	free_list(out);
    free_list(out2);
	free_adjacency(g);

	t2=time(NULL);
	printf("\n- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}