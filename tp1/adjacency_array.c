#include <stdlib.h>
#include <stdio.h>
#include <time.h>//to estimate the runing time
#include "adjacency_array.h"

#define NLINKS 1000000 //maximum number of edges for memory allocation, will increase if needed

unsigned long max2(unsigned long u,unsigned long v);

void read(adjacency *g){
	for (unsigned long i= 0 ; i < g->maxn ; i++){
		printf("%lu: ",i);		
		for (unsigned long j= 0 ; j < g->nodes[i].size ; j++){
			printf("%lu ",g->nodes[i].s[j]);										
		}
		if (g->nodes[i].size == 0){
			printf("no neighbors");
		}
		printf("\n");
	}
}

unsigned long max2(unsigned long u,unsigned long v){
    return (u>v) ? u : v;
}

int main(int argc,char** argv){
	adjacency *g;
	time_t t1,t2;

	t1=time(NULL);

	printf("Reading edges from file %s\n",argv[1]);
	g=readadjacency(argv[1]);
	read(g);	

	printf("Number of nodes: %lu\n",g->maxn);

	free_adjacency(g);

	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}
