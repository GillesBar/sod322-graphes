#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/*gnuplot
set logscale y
set xlabel
set xlabel
plot 'distribution_email-Eu-core.txt' u 1:2
*/

#define MAXDEGREE 1000

int main(int argc,char** argv){

	unsigned long *distribution = calloc(MAXDEGREE,sizeof(unsigned long));
	unsigned long u = 0;
	unsigned long current_limit = MAXDEGREE;
	unsigned long max = 0;
	time_t t1,t2;

	t1=time(NULL);

    char filename[80] = "degree_";
    strcat(filename, argv[1]);
    FILE *degrees_file = fopen(filename,"r");
    char outname[80] = "distribution_";
    strcat(outname, argv[1]);
    FILE *out = fopen(outname,"w");

	printf("Reading degrees from file %s\n",filename);

	while (fscanf(degrees_file,"%lu", &u) == 1) {
		while (u >= current_limit){
            distribution = realloc(distribution,2*current_limit*sizeof(unsigned long));            
            for (int i = current_limit ; i < 2* current_limit ; i++){
                distribution[i] = 0;                
            }
            current_limit *= 2; 
        }		
		distribution[u] ++;
		if (u > max){
			max = u;
		}
	}
	
	printf("Writing degrees to file %s\n",outname);    
	for (int i = 0 ; i <= max ; i++){	   	  
	    fprintf(out,"%u %lu\n",i,distribution[i]);
	}
	
	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));
	
	free(distribution);
	fclose(degrees_file);
	fclose(out);

	return 0;
}
