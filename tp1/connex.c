#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "BFS.h"

int main(int argc,char** argv){

	time_t t1,t2;

	t1=time(NULL);

	printf("Reading edges from file %s\n",argv[1]);
	adjacency *g=readadjacency(argv[1]);

    unsigned long *length = calloc((g->maxn+1),sizeof(unsigned long));
    unsigned long *marked = calloc((g->maxn+1),sizeof(unsigned long));
	unsigned long iter = 1;
    unsigned long treated = 1; 
    unsigned long last_treated = 0; 
    unsigned long i = 0;
    unsigned long temp;

    while (g->maxn > treated - 1 && treated - last_treated > 0){ // To avoid being trapped at an isolated point
        last_treated = treated;
        while (marked[i] != 0 && iter != 1){
            i++;
        } 
        if (i >= g->maxn){
            break;
        }
        list *out = BFS(g,i);        
    
	    for (unsigned long j = 0 ; j < out->size ; j++){
            temp = out->values[j];
		    marked[temp] = iter;
	    }
        length[iter-1] = out->size;
        treated += out->size;
        iter ++;
        free_list(out); 
        i = 0;            
    }	
	
    for (unsigned long k = 0 ; k < iter - 1 ; k ++){
        if (length[k] != 1){
            printf("Size %lu, proportion %f  ",length[k],(float)length[k]/g->number_nodes);
        }
    }

    //for (unsigned long k = 0 ; k < g->maxn; k ++){
    //    printf("Mark %lu\n",marked[k]);
    //}

    free(length);
    free(marked);
	free_adjacency(g);

	t2=time(NULL);
	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}