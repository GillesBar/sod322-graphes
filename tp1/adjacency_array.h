#define NLINKS 1000000

typedef struct {
	unsigned long max;
	unsigned long size;
	unsigned long *s;
} neighbors;

//adjacency structure:
typedef struct {
	unsigned long maxn;//max index
	unsigned long number_nodes;
	neighbors *nodes;//list of neighbors
} adjacency;

//compute the maximum of three unsigned long
unsigned long max3(unsigned long a,unsigned long b,unsigned long c){
	a=(a>b) ? a : b;
	return (a>c) ? a : c;
}

unsigned long max2(unsigned long u,unsigned long v);

void free_adjacency(adjacency *g);

void read(adjacency *g);

//reading the adjacency array from file
adjacency* readadjacency(char* input){
	unsigned long e1=NLINKS;
	unsigned long u;
	unsigned long v;
	//int *marked = calloc(e1,sizeof(int));

	FILE *file=fopen(input,"r");

	adjacency *g = malloc(sizeof(adjacency));
	g->maxn = 0;
	g->number_nodes = 0;
	g->nodes = malloc(e1*sizeof(neighbors));

	for (unsigned long i = 0 ; i < e1 ; i++){
		g->nodes[i].max = 10;
		g->nodes[i].size = 0;
		g->nodes[i].s = calloc(10,sizeof(unsigned long));
	}

	while (fscanf(file,"%lu %lu", &u, &v) == 2) {
		g->maxn = max3(g->maxn,u,v);
		while (g->maxn >  e1) {//increase allocated RAM if needed
			e1 += NLINKS;
			g->nodes = realloc(g->nodes,e1*sizeof(neighbors));
			//marked = realloc(e1,sizeof(int));
			//for (unsigned long i = e1 - NLINKS ; i < e1 ; i ++ ){
			//	marked[i] = 0;
			//}
		}
		if (g->nodes[u].size >= g->nodes[u].max) {//increase allocated RAM if needed
			g->nodes[u].max  += 100;			
			g->nodes[u].s = realloc(g->nodes[u].s,g->nodes[u].max*sizeof(unsigned long));
		}
		if (g->nodes[v].size >= g->nodes[v].max) {//increase allocated RAM if needed
			g->nodes[v].max  += 100;			
			g->nodes[v].s = realloc(g->nodes[v].s,g->nodes[v].max*sizeof(unsigned long));
		}
		g->nodes[u].s[g->nodes[u].size] = v;
		g->nodes[u].size  ++;
		g->nodes[v].s[g->nodes[v].size] = u;
		g->nodes[v].size  ++;
	}

	for (unsigned long i = 0 ; i <= g->maxn ; i++){
		if (g->nodes[i].size >0){			
			g->number_nodes ++;
		}
	}
	//free(marked);
	fclose(file);

	return g;
}

void free_adjacency(adjacency *g){
	for (unsigned long i= 0 ; i < g->maxn ; i++){
		free(g->nodes[i].s);	
	}
	free(g->nodes);
	free(g);
}
