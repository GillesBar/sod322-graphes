// TO DEBUG

#include <stdlib.h>
#include <stdio.h>
#include <time.h>//to estimate the runing time

#define NLINKS 1000000 //maximum number of edges for memory allocation, will increase if needed

unsigned long max2(unsigned long u,unsigned long v);

//adjacency structure:
typedef struct {
	unsigned long maxn;//number of nodes
	unsigned long size;//size of matrix
	int *nodes;//list of neighbors
} adjacency_matrix;

//compute the maximum of three unsigned long
inline unsigned long max3(unsigned long a,unsigned long b,unsigned long c){
	a=(a>b) ? a : b;
	return (a>c) ? a : c;
}

int* access(adjacency_matrix *g, unsigned long i, unsigned long j){
	return &(g->nodes[j+g->size*i]);
}

void free_adjacency_matrix(adjacency_matrix *g){
	free(g->nodes);
	free(g);
}

//reading the adjacency array from file
adjacency_matrix* readadjacency_matrix(char* input){
	unsigned long e1=NLINKS;
	unsigned long u;
	unsigned long v;

	FILE *file=fopen(input,"r");
	printf("test");

	adjacency_matrix *g = malloc(sizeof(adjacency_matrix));
	g->maxn = 0;
	g->size = e1;
	g->nodes = calloc(e1*e1,sizeof(int));

	while (fscanf(file,"%lu %lu", &u, &v) == 2) {
		g->maxn = max3(g->maxn,u,v);
		printf("test");
		while (g->maxn >=  g->size) {//increase allocated RAM if needed
			e1 += NLINKS;			
			adjacency_matrix *h = malloc(sizeof(adjacency_matrix));
			h->maxn = g->maxn;
			h->size = e1;
			h->nodes = calloc(e1*e1,sizeof(int));
			for (unsigned long i = 0 ; i < g->size ; i ++){
				for (unsigned long j = 0 ; j < g->size ; j++){
					*access(h,i,j) = *access(g,i,j);
				}
			}
			g->size = e1;
			g->nodes = calloc(e1*e1,sizeof(int));
			*g->nodes = *h->nodes;
			free_adjacency_matrix(h);
		}
		*access(g,u,v) = 1;
		*access(g,v,u) = 1;
	}
	fclose(file);

	adjacency_matrix *h = malloc(sizeof(adjacency_matrix));
	h->maxn = g->maxn;
	h->size = g->maxn;
	h->nodes = calloc(g->maxn*g->maxn,sizeof(int));
	for (unsigned long i = 0 ; i < g->maxn ; i ++){
		for (unsigned long j = 0 ; j < g->maxn ; j++){
			*access(h,i,j) = *access(g,i,j);
		}
	}
	free_adjacency_matrix(g);

	return h;
}

void read(adjacency_matrix *g){
	for (unsigned long i = 0 ; i < g->maxn ; i ++){
		for (unsigned long j = 0 ; j < g->maxn ; j++){
			printf("%d ",*access(g,i,j));
		}
		printf("\n");
	}
}

unsigned long max2(unsigned long u,unsigned long v){
    return (u>v) ? u : v;
}


int main(int argc,char** argv){
	adjacency_matrix *g;
	time_t t1,t2;

	t1=time(NULL);

	printf("Reading edges from file %s\n",argv[1]);
	g=readadjacency_matrix(argv[1]);
	//read(g);	

	printf("Number of nodes: %lu\n",g->maxn);

	free_adjacency_matrix(g);

	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}
