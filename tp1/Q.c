#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define NLINKS 1000000

int main(int argc,char** argv){

	unsigned long *degrees = calloc(NLINKS,sizeof(unsigned long));
	unsigned long u = 0;
	unsigned long v = 0;
	unsigned long index = 0;
	unsigned long current_limit = NLINKS;
	unsigned long Q = 0;
	time_t t1,t2;

	t1=time(NULL);

	FILE *file = fopen(argv[1],"r");
    char filename[80] = "degree_";
    strcat(filename, argv[1]);
    FILE *degrees_file = fopen(filename,"r");

	printf("Reading degrees from file %s\n",filename);

	while (fscanf(degrees_file,"%lu", &u) == 1) {
		if (index >= current_limit){
            degrees = realloc(degrees,2*current_limit*sizeof(unsigned long));            
            for (int i = current_limit ; i < 2* current_limit ; i++){
                degrees[i] = 0;                
            }
            current_limit *= 2;            
        }
		degrees[index] = u;
		index ++;
	}
	
	printf("Reading edgelist from file %s\n",argv[1]);
	while (fscanf(file,"%lu %lu", &u, &v) == 2) {
		Q += degrees[u]*degrees[v];	
	}
	
	t2=time(NULL);

	printf("Q = %lu\n",Q);
	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));
	
	free(degrees);

	fclose(file);
	fclose(degrees_file);

	return 0;
}
