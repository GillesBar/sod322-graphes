#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "BFS.h"

unsigned long max2(unsigned long u,unsigned long v);

int main(int argc,char** argv){
    unsigned long n = 0;
    unsigned long start0 = 0;
    unsigned long u;
    unsigned long v;
    unsigned long temp;
    unsigned long edges = 0;
	time_t t1,t2;

	t1=time(NULL);
    FILE *file=fopen(argv[1],"r");
    printf("Reading edgelist from file %s\n",argv[1]);
    
    while (fscanf(file,"%lu %lu", &u, &v)==2) {
        edges ++;
	}

    adjacency *g=readadjacency(argv[1]);
	t2=time(NULL);
	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));
	
    printf("Number of nodes: %lu\n",g->number_nodes);
    printf("Number of edges: %lu\n",edges);
    //printf("%lu\n",start0);
    free_adjacency(g);
    fclose(file);
	}

unsigned long max2(unsigned long u,unsigned long v){
    return (u>v) ? u : v;
}
