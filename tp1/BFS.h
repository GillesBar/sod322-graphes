#include "adjacency_array.h"

typedef struct {
	unsigned long size;
	unsigned long *values;
	unsigned long *marked;
} list;

void free_list(list *l){
	free(l->values);
	free(l->marked);
	free(l);
}

list* BFS(adjacency *g,unsigned long s){
	unsigned long *temp = malloc((g->maxn+1)*sizeof(unsigned long));
	unsigned long n = 0;
	unsigned long u;
	unsigned long *stack = malloc((g->maxn+1)*sizeof(unsigned long));
	unsigned long *marked = calloc((g->maxn+1),sizeof(unsigned long));
	unsigned long last = 1;
	unsigned long first = 0;
	marked[s] = 1;
	stack[0] = s;
	
	while (last > first){
		u = stack[first];
		first ++;
		temp[n] = u;
		n++;
		for (unsigned long i = 0 ; i < g->nodes[u].size ; i++){
			if (marked[g->nodes[u].s[i]] == 0){
				stack[last] = g->nodes[u].s[i];
				marked[g->nodes[u].s[i]] = marked[u] + 1;	
				last ++;
			}
		}
	}
	list *out;
	out = malloc(sizeof(list));
	out->size = n;
	out->values = malloc(n*sizeof(unsigned long));
	out->marked = malloc(n*sizeof(unsigned long));
	
	for (unsigned long i = 0 ; i < n ; i++){
		out->values[i] = temp[i];
		out->marked[i] = marked[i];
	}
	free(temp);
	free(stack);
	return out;
}