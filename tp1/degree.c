#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define NLINKS 1000000 //maximum number of edges for memory allocation, will increase if needed

unsigned long max2(unsigned long u,unsigned long v);

int main(int argc,char** argv){
    unsigned long *degrees = calloc(NLINKS,sizeof(unsigned long));
    
    if (degrees == NULL){
        printf("Memory allocation failure");
        return 1;
    }
    
	time_t t1,t2;
	t1=time(NULL);

    FILE *file = fopen(argv[1],"r");
    char filename[80] = "degree_";
    strcat(filename, argv[1]);
    FILE *out = fopen(filename,"w");

    unsigned long u = 0;
    unsigned long v = 0;
    unsigned long nodes = 0;
    unsigned long current_limit = NLINKS;

	printf("Reading edgelist from file %s\n",argv[1]);
    
    while (fscanf(file,"%lu %lu", &u, &v) == 2) {
        nodes = max2(max2(u,nodes),v); 
        
        while (nodes >= current_limit){
            degrees = realloc(degrees,2*current_limit*sizeof(unsigned long));            
            for (int i = current_limit ; i < 2* current_limit ; i++){
                degrees[i] = 0;                
            }
            current_limit *= 2;            
        }
        
        degrees[u] ++;
        degrees[v] ++;
	}	
	printf("Writing degrees to file %s\n",filename);    
	for (int i = 0 ; i < nodes ; i++){	   	  
	    fprintf(out,"%lu\n",degrees[i]);
	}
	t2=time(NULL);
	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));
	free(degrees);	
	
	fclose(file);
	fclose(out);
	return 0;
}

unsigned long max2(unsigned long u,unsigned long v){
    return (u>v) ? u : v;
}
