import networkx as nx
import matplotlib.pyplot as plt

fh = open("random_graph_0.1_0.01", 'rb')
G = nx.read_edgelist(fh)
fh.close()

nx.draw_networkx(G)
plt.draw()
plt.show()