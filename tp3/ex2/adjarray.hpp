#ifndef ADJARRAYSET_HPP
#define ADJARRAYSET_HPP

#include <vector>
#include <unordered_set>
#include <map>
#include <string>
#include <fstream>
#include <iostream>

typedef struct {
    unsigned int n;
    unsigned int m;
    std::map<unsigned int, std::unordered_set<unsigned int>> adjarr;
} AdjArray;



AdjArray adjarray_from_txt(std::string filepath, bool isdirected) {

    std::ifstream graphFile;
    graphFile.open(filepath, std::ifstream::in);

    unsigned int m = 0;

	// Get number of comments
	std::string line;

	int nb_comments = 0;
	getline(graphFile, line);
	while (line[0] == '#') {
		getline(graphFile, line); nb_comments++;
	}

	// Reset to file begining, jump comments, read graph
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) getline(graphFile, line);
	
    std::map<unsigned int, std::unordered_set<unsigned int>> adjarr;

	unsigned int i, j;
	while (graphFile >> i >> j) {
        adjarr[i].insert(j);
        m++;

        if (!isdirected) {
            adjarr[j].insert(i);
        }
	}

	graphFile.close();

    return {(unsigned int) adjarr.size(), m, adjarr};
}

void show(AdjArray &aa) {
    std::cout << "- m = " << aa.m << '\n';
    std::cout << "- n = " << aa.n << '\n';
    
    for ( auto it = aa.adjarr.begin(); it != aa.adjarr.end(); it++ )
    {
        std::cout << "Node " << it->first << std::endl;
        for (auto set_it : it->second) {
            std::cout << set_it << "  ";
        }
        std::cout << '\n';
    }

}

#endif // !ADJARRAYSET_HPP