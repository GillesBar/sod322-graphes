#include <time.h>//to estimate the runing time
#include <string>
#include <vector>
#include <unordered_set>
#include <ostream>
#include <sstream>
#include <algorithm>    // std::random_shuffle

#include "adjarray.hpp"
#include <math.h>       /* sqrt */


std::vector<int> label_propagation(const AdjArray &adjarr) {
	unsigned int max_vert = 0;

	for (const auto kv : adjarr.adjarr) {
		max_vert = std::max(max_vert, kv.first);
		max_vert = std::max(max_vert, *(std::max_element(kv.second.begin(), kv.second.end())));
	}
	// std::cout << "max vertex:"  << max_vert << '\n';

	// std::cout << "Max vert: " << max_vert << '\n';
	std::vector<int> vertex_to_label(max_vert+1, -1);
	std::vector<unsigned int> ind_to_vertex(max_vert+1, 0);

	for (size_t ind=0; ind<vertex_to_label.size(); ind++) {
		ind_to_vertex[ind] = ind;
		vertex_to_label[ind] = ind;
	}

	// std::cout << "ind_to_vert:" << '\n';
	// for (auto v : ind_to_vertex)
		// std::cout << v << "  " << '\n';

	bool hasconverged = false;

	while (!hasconverged) {
		hasconverged = true;
		// std::cout << "\n\n--- Iteration" << '\n';
		// std::cout << "Vertex_to_label" << '\n';
		// for (int ind=0; ind<vertex_to_label.size(); ind++) {
			// std::cout << ind << "\t" << vertex_to_label[ind] << '\n';
		// }

		// Draw random permutation
		std::random_shuffle ( ind_to_vertex.begin(), ind_to_vertex.end() );

		// std::cout << "ind_to_vert" << '\n';
		// for (int ind=0; ind<ind_to_vertex.size(); ind++) {
		// 	std::cout << ind << "\t" << ind_to_vertex[ind] << '\n';
		// }
		// std::cout << "" << '\n';


		// Update labels
		for (unsigned int vert : ind_to_vertex) {
			// std::cout << "* Dealing with " << vert << '\n';

			// count frequency of neighborhood labels
			std::map<int, int> label_to_freq;
			auto neighs = adjarr.adjarr.find(vert);
			// std::cout << (neighs == adjarr.adjarr.end()) << '\n';

			if (neighs != adjarr.adjarr.end()) {// vertex ind corresponds to an actual vertex...
				// count
				for (auto neigh : (*neighs).second) {
					std::map<int, int>::iterator it = label_to_freq.find(vertex_to_label[neigh]);
					if (it == label_to_freq.end()) 
					{
						label_to_freq[vertex_to_label[neigh]] = 1;
					} else
					{
						(*it).second += 1;
					}
				}

				// //// print frequency it
				// for (auto lf : label_to_freq)
				// 	std::cout << lf.first << "\t"  << lf.second << '\n';

				// Get maximum frequency
				auto max_lab_freq = std::max_element(label_to_freq.begin(), label_to_freq.end(),
					[](const std::pair<int, int>& p1, const std::pair<int, int>& p2) {
						return p1.second < p2.second; 
					});

				// std::cout << "max elem " << (*max_lab_freq).first << "  " << (*max_lab_freq).second << '\n';

				// Set element label if need be
				if (vertex_to_label[vert] != (*max_lab_freq).first) {
					vertex_to_label[vert] = (*max_lab_freq).first;
					hasconverged = false;
				}

			}
			
		}
	}

	return vertex_to_label;
}

int main(int argc,char** argv){
	time_t t1,t2;

	t1=time(NULL);

	if (argc!=3) {
		std::cout << "Expected input:" << '\n';
		std::cout << "   - edgelist file path" << '\n';
		std::cout << "   - label prop output filename" << '\n';
		return 0;
	}

	// Load graph data
	std::string filePath = std::string(argv[1]);
	AdjArray adjarr = adjarray_from_txt(filePath, false);

	std::cout << "n = " << adjarr.n << '\n';
	std::cout << "m = " << adjarr.m << '\n';
	std::cout << "el length = " << adjarr.adjarr.size() << '\n';
	
	// run label propagation
	std::vector<int> vertex_to_label = label_propagation(adjarr);


	// write result
	std::ofstream output_file(std::string(argv[2]), std::ios::ate);

	int ind = 0;
	for (auto it_label = vertex_to_label.begin(); it_label!=vertex_to_label.end(); ind++, it_label++) {
		// if (*it_label != 0) {
			// output_file << ind << std::endl;
			output_file << std::to_string(ind) + "," + std::to_string(*it_label) << std::endl;
		// }
	}
	output_file.close();

	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}