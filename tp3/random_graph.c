#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define NODES 400
#define CLUSTER 4

int main(int argc,char** argv){

	time_t t1,t2;
    int label[NODES];
    float random;

    float p = atof(argv[1]);
    float q = atof(argv[2]);

    for (int i = 0 ; i < NODES ; i++){
        if (i<NODES/CLUSTER){
            label[i] = 1;
        }
        else if (i<2*NODES/CLUSTER){
            label[i] = 2;
        }
        else if (i<3*NODES/CLUSTER){
            label[i] = 3;
        }
        else{
            label[i] = 4;
        }
    }

	t1=time(NULL);

    srand(t1);
    char outname[80] = "random_graph_";
    strcat(outname, argv[1]);
    strcat(outname, "_");
    strcat(outname, argv[2]);

	printf("Writing edges in file %s",outname);	
    FILE *file=fopen(outname,"w");

    for (int i = 0 ; i < NODES ; i ++){
        for (int j = i + 1; j < NODES ; j ++){
            random = (float) rand()/RAND_MAX;
            if (label[i] == label[j]){
                if (random < p){
                    fprintf(file,"%d %d\n",i,j);
                }
            }
            else {
                if (random < q){
                    fprintf(file,"%d %d\n",i,j);
                }
            }            
        }
    }

    fclose(file);

	t2=time(NULL);
	printf("\n- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}