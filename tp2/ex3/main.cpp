#include <time.h>//to estimate the runing time
#include <string>
#include <vector>
#include <unordered_set>
#include <set>
#include <map>
#include <unordered_map>
#include <sstream>
#include <algorithm>    // std::partial_sort_copy

#include "../edgeList.hpp"
#include "../adjArray.hpp"
#include "../adjarray.hpp"
#include <math.h>       /* sqrt */

typedef struct {
	unsigned int vertex;
	unsigned int degree;
} vertdeg;

void kcore_decomposition_ar(AdjArray& adjarr, std::vector<unsigned int>& ind2corevalue, std::vector<unsigned int>& ind2korder) {
	int n = adjarr.n;

	// Set vectors to right size and 0 TODO: set max vertex ind here.
	ind2corevalue.resize(n+1);
	ind2korder.resize(n+1);
	std::fill(ind2corevalue.begin(), ind2corevalue.end(), 0);
	std::fill(ind2korder.begin(), ind2korder.end(), 0);

	int i = n;
	unsigned int c = 0;

	std::set<unsigned int> valid_indices;
	std::vector<vertdeg> ind_to_vertdeg(n, {0, 0});
	std::unordered_map<unsigned int, unsigned int> vertex_to_ind;
	
	// populating vertex set, edge set and degrees
	// std::cout << "Populating vertex set, edge set and degrees...\n";

	int ind = 0;
	for ( auto it = adjarr.adjarr.begin(); it != adjarr.adjarr.end(); it++ )
    {
		ind_to_vertdeg[ind] = {it->first, (unsigned int) (it->second).size()};
		valid_indices.insert(ind);

		ind++;
    }

	////// Printing
	// std::cout << "ind | vertex | degree" << std::endl;
	// for ( auto ind : valid_indices )
  //   {
	// 	std::cout << ind << "\t" << ind_to_vertdeg[ind].vertex << "\t" << ind_to_vertdeg[ind].degree << '\n';
  //   }

	// Sorting (vertex, degree) per decreasing.
	std::sort(ind_to_vertdeg.begin(), ind_to_vertdeg.end(), [](const vertdeg& a, const vertdeg& b) {
        return a.degree < b.degree;
    });

	for (auto ind : valid_indices) {
		unsigned int vertex = ind_to_vertdeg[ind].vertex;
		vertex_to_ind[vertex] = ind;
	}

	// ////// Printing
	// std::cout << "ind | vertex | degree" << std::endl;
	// for ( auto ind : valid_indices )
  //   {
	// 	std::cout << ind << "\t" << ind_to_vertdeg[ind].vertex << "\t" << ind_to_vertdeg[ind].degree << '\n';
  //   }
	
	// std::cout << " done." << '\n';

	// main iteration
	while (!valid_indices.empty()) {
	// while (valid_indices.size() > 3) {
		// std::cout << "\n\n\n\n---- Iteration" << '\n';
		// std::cout << "c:		" << c << '\n';
		// std::cout << "i:		" << i << '\n';
		// std::cout << "Vertex:  	" << vertex_set.size() << '\n';
		// for (auto v : vertex_set) std::cout << v << "  ";
		// std::cout << "\nEdges:   	" << edge_set.size() << '\n';
		// for (auto e : edge_set) std::cout << "{" << e.s << "," << e.t << "},  ";
		// std::cout << '\n';
		// std::cout << "Degrees:  	" << '\n';
		// for (int i=0; i<vertex_degree.size(); i++) std::cout << i << "\t" << vertex_degree[i] << "\n";

		// std::cout << "ind | vertex | degree" << std::endl;
		// for ( auto ind : valid_indices )
		// {
		// 	std::cout << ind << "\t" << ind_to_vertdeg[ind].vertex << "\t" << ind_to_vertdeg[ind].degree << '\n';
		// }

		// clock_t begin = clock();

		// Compute min degree vertex
		unsigned int ind_vertdeg_mindeg = *(valid_indices.begin());
		vertdeg vertdeg_mindeg = ind_to_vertdeg[ind_vertdeg_mindeg];

		// clock_t interm = clock();

		// std::cout << "ind_vertdeg_mindeg:   " << ind_vertdeg_mindeg << '\n';
		// std::cout << "\nMinimum degree vertex: " << vertdeg_mindeg.vertex << "  with degree " << vertdeg_mindeg.degree << '\n';
		c = std::max(c, vertdeg_mindeg.degree);

		// erase v_mindeg; update relevant degrees and restore sort
		valid_indices.erase(ind_vertdeg_mindeg);

		for (auto neigh_vert : adjarr.adjarr[vertdeg_mindeg.vertex]) {
			unsigned int neigh_ind = vertex_to_ind[neigh_vert];
			unsigned int neigh_deg = ind_to_vertdeg[neigh_ind].degree;

			// std::cout << "+++++++ Dealing with neigh " << neigh_vert << "  degree: " << neigh_deg << "  index: " << neigh_ind << '\n';

			// std::cout << "\n**Before degree decrease" << '\n';
			// std::cout << "ind | vertex | degree" << std::endl;
			// for ( auto ind : valid_indices )
			// {
			// 	std::cout << ind << "\t" << ind_to_vertdeg[ind].vertex << "\t" << ind_to_vertdeg[ind].degree << '\n';
			// }
			// std::cout << "\n**\n";

			// If vertex is still in graph
			if (valid_indices.find(neigh_ind) != valid_indices.end()) {
				// std::cout << "** " << '\n';

				unsigned int infstric_ind = neigh_ind-1;
				while ((ind_to_vertdeg[infstric_ind].degree == neigh_deg) && (valid_indices.find(infstric_ind+1) != valid_indices.begin())) { 
					// std::cout << "Current index: " << infstric_ind << ";  degree: " << ind_to_vertdeg[infstric_ind].degree << "  vertex: " << ind_to_vertdeg[infstric_ind].vertex << '\n';
					infstric_ind--;
				}
				// std::cout << "Current index: " << infstric_ind << ";  degree: " << ind_to_vertdeg[infstric_ind].degree << "  vertex: " << ind_to_vertdeg[infstric_ind].vertex << '\n';

				// Permute ind_neigh and ind_infstrict+1
				ind_to_vertdeg[neigh_ind] = ind_to_vertdeg[infstric_ind+1];
				ind_to_vertdeg[infstric_ind+1] = {neigh_vert, neigh_deg-1};

				vertex_to_ind[neigh_vert] = infstric_ind+1;
				vertex_to_ind[ind_to_vertdeg[neigh_ind].vertex] = neigh_ind;
			}

			// std::cout << "\n**After degree decrease" << '\n';
			// std::cout << "ind | vertex | degree" << std::endl;
			// for ( auto ind : valid_indices )
			// {
			// 	std::cout << ind << "\t" << ind_to_vertdeg[ind].vertex << "\t" << ind_to_vertdeg[ind].degree << '\n';
			// }
			// std::cout << "\n**\n";
		}

		// std::cout << "\n\nEnd of iteration" << '\n';
		// std::cout << "ind | vertex | degree" << std::endl;
		// for ( auto ind : valid_indices )
		// {
		// 	std::cout << ind << "\t" << ind_to_vertdeg[ind].vertex << "\t" << ind_to_vertdeg[ind].degree << '\n';
		// }



		// for (auto e_it = edge_set.begin(); e_it != edge_set.end(); ++e_it) {
		// 	std::cout << "Edge is " << "{" << (*e_it).s << "," << (*e_it).t << "},  " << '\n';
		// 	if ((*e_it).s == v_mindeg) {
		// 		std::cout << "/* message */ -- s" << '\n';
		// 		edge_set.erase(e_it);
		// 		vertex_degree[(*e_it).t]--;

		// 		e_it++;
		// 	}

		// 	if ((*e_it).t == v_mindeg) {
		// 		std::cout << "/* message */ -- t" << '\n';

		// 		edge_set.erase(e_it);
		// 		vertex_degree[(*e_it).s]--;

		// 		e_it++;
		// 	}
		// }

		// Set node corevalue and core ordering index
		ind2corevalue[vertdeg_mindeg.vertex] = c;
		ind2korder[vertdeg_mindeg.vertex] = i;
		i--;

		// clock_t end = clock();
		// std::cout << double(interm - begin) / CLOCKS_PER_SEC << '\n';
		// std::cout << double(end - interm) / CLOCKS_PER_SEC << '\n';

	}
}

int main(int argc,char** argv){
	time_t t1,t2;

	t1=time(NULL);

	double alpha = 0.15;
	double tol = 1e-8;

	if (argc==1) {
		std::cout << "Expected input: edgelist file path" << '\n';
		return 0;
	}


	// Load graph data
	std::string filePath = std::string(argv[1]);
	// edgelist el = edgelist_from_txt(filePath);
	AdjArray adj_array =  adjarray_from_txt(filePath, false);

	std::cout << "n = " << adj_array.n << '\n';
	std::cout << "m = " << adj_array.m << '\n';
	std::cout << "el length = " << adj_array.adjarr.size() << '\n';

	// Running k-core algo
	std::vector<unsigned int> ind2corevalue(adj_array.n, 0);
	std::vector<unsigned int> ind2korder(adj_array.n, 0);
	kcore_decomposition_ar(adj_array, ind2corevalue, ind2korder);

	std::cout << "ind2corevalue" << '\n';
	for (auto kv : ind2corevalue) std::cout << kv << "  ";

	std::cout << "\nind2korder" << '\n';
	for (auto ko : ind2korder) std::cout << ko << "  ";
	std::vector<unsigned int>::iterator res = std::max_element(ind2corevalue.begin(), ind2corevalue.end());
	std::cout << "Maximum core value: " << *res << '\n';
	
	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}