#ifndef EDGELIST_HPP
#define EDGELIST_HPP

#include <vector>
#include <string>
#include <fstream>
#include <iostream>


typedef struct {
	unsigned long s;
	unsigned long t;
} edge;

inline bool operator< (const edge& lhs, const edge& rhs){ return ((lhs.s < rhs.s) || ((lhs.s == rhs.s) && (lhs.t < rhs.t))); }

class EdgeHash { 
public: 
    size_t operator()(const edge& e) const
    { 
        return e.s ^ e.t; 
    } 
};

struct edge_hash : public std::unary_function<edge, std::size_t>
{
	std::size_t operator()(const edge& e) const
	{
		std::cout << "edge_hash" << '\n';
		return e.s ^ e.t;
	}
};
 
struct edge_equal : public std::binary_function<edge, edge, bool>
{
	bool operator()(const edge& e0, const edge& e1) const
	{
		std::cout << "edge_equal" << '\n';
		return (e0.s == e1.s && e0.t == e1.t);
	}
};

//edge list structure:
typedef struct {
	unsigned long n;//number of nodes
	unsigned long m;//number of edges
	std::vector<edge> edges;
} edgelist;



edgelist edgelist_from_txt(std::string filepath) {

    std::ifstream graphFile;
    graphFile.open(filepath, std::ifstream::in);

    unsigned int m = 0;
    unsigned int n = 0;

	// Get number of comments
	std::string line;

	int nb_comments = 0;
	getline(graphFile, line);
	while (line[0] == '#') {
		getline(graphFile, line); nb_comments++;
	}

	// Reset to file begining, jump comments, read graph
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) getline(graphFile, line);
	
	unsigned int i, j;
	while (graphFile >> i >> j) {
		m++;
		n = std::max(n, i);
		n = std::max(n, j);
	}
	n += 1;

	std::cout << "Input graph has n,m = " << n << "," << m << ". Loading edges..." << '\n';

    std::vector< edge> edges(m, {0, 0});

    // Populate edges
	graphFile.clear();
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) {
		getline(graphFile, line);
	}
	
    size_t ind = 0;
	while (graphFile >> i >> j) {
        edges[ind] = {i, j};
        ind++;
	}

	graphFile.close();

    return {n, m, edges};
}

void show(edgelist &el) {
    std::cout << "- m = " << el.m << '\n';
    std::cout << "- n = " << el.n << '\n';
    std::cout << "- Edge list" << '\n';
    for (auto it = el.edges.begin(); it != el.edges.end(); it++) {
        std::cout << (*it).s << "  " << (*it).t << '\n';
    }
}

#endif // !EDGELIST_HPP