#ifndef ADJARRAY_HPP
#define ADJARRAY_HPP

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

typedef struct {
    unsigned int n;
    unsigned int m;
    std::vector<unsigned int> cd;
    std::vector<unsigned int> adj;
} adjarray;



adjarray adjarray_from_txt(std::string filepath) {

    std::ifstream graphFile;
    graphFile.open(filepath, std::ifstream::in);

    unsigned int m = 0;
    unsigned int n = 0;

	// Get number of comments
	std::string line;

	int nb_comments = 0;
	getline(graphFile, line);
	while (line[0] == '#') {
		getline(graphFile, line); nb_comments++;
	}

	// Reset to file begining, jump comments, read graph
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) getline(graphFile, line);
	
	unsigned int i, j;
	while (graphFile >> i >> j) {
		m++;
		n = std::max(n, i);
		m++;
		n = std::max(n, j);
	}

    std::vector<unsigned int> cd(n+1, 0);
    std::vector<unsigned int> temp_indices(n+1, 0);
    std::vector<unsigned int> adj(m, 0);

    // Compute degree per edge
	graphFile.clear();
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) {
		getline(graphFile, line);
	}
	
	while (graphFile >> i >> j) {
        cd[i] += 1;
        cd[j] += 1;
	}

    for (size_t ind=1; ind<cd.size(); ind++) {
        cd[ind] += cd[ind-1];
    }

    // Populate edges
	graphFile.clear();
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) {
		getline(graphFile, line);
	}
	
	while (graphFile >> i >> j) {
        adj[cd[i] + temp_indices[i]] = i;
        temp_indices[i] += 1;

        // adj[cd[j] + temp_indices[j]] = i;
        // temp_indices[j] += 1;
	}


	graphFile.close();

    return {n, m, cd, adj};
}

void show(adjarray &aa) {
    std::cout << "- m = " << aa.m << '\n';
    std::cout << "- n = " << aa.n << '\n';
    std::cout << "- Adjacency array" << '\n';
    std::cout << "vertex  | cumulative degree | neighboor" << '\n';

	size_t adj_ind = 0;
	size_t loc_ind = 0;
	
	std::cout << "Cumulative degree" << '\n';
	for (auto it = aa.cd.begin(); it != aa.cd.end(); it++) {
        std::cout << adj_ind << "    " << (*it) << '\n';
    }

	adj_ind = 0;
	std::cout << "\nAdjacency array" << '\n';
    for (auto it = aa.adj.begin(); it != aa.adj.end(); it++, adj_ind++, loc_ind++) {
        std::cout << adj_ind << "    " << (*it) << '\n';
    }
}

#endif // !ADJARRAY_HPP