#ifndef PAGERANK_HPP
#define PAGERANK_HPP

#include <vector>

#include <math.h>       /* sqrt */
#include "edgeList.hpp"


double get_norm1(std::vector<double> &vec) {
	double norm = 0;
	for(size_t i = 0; i < vec.size(); i++)
	{
		norm += vec[i];
	}
	return norm;
}

std::vector<double> PageRank(edgelist &el, double alpha, double tol) {

	unsigned int it_max = 200;

	double n = (double) el.n;

	std::vector<double> p((int) el.n, 1/n);
	std::vector<double> p_temp((int) el.n);
	std::vector<double> p_old((int) el.n);
	double step_norm = 1e20;

	// Compute out degrees
	std::vector<unsigned int> d_out(el.n, 0);

	for (auto it = el.edges.begin(); it != el.edges.end(); it++) {
		d_out[(*it).s] += 1;
	}

	size_t i = 0;
	while ((step_norm > tol) && (i < it_max))
	{
		std::cout << "it: " << i;

		// save previous point
		for(size_t ind = 0; ind < p.size(); ind++) {
			p_old[ind] = p[ind];
		}

		// P_temp = T*P
		for(size_t ind = 0; ind < p_temp.size(); ind++) {
			p_temp[ind] = 0;
		}
		for(size_t edge_ind = 0; edge_ind < el.edges.size(); edge_ind++)
		{
			edge e = el.edges[edge_ind];
			p_temp[e.t] += p[e.s] / (double)d_out[e.s];
		}

		// P = (1-alpha) * P_temp + alpha*1/n
		for(size_t ind = 0; ind < p.size(); ind++) {
			p[ind] = (1-alpha) * p_temp[ind] + alpha / n;
		}
		
		// Compute p l1 norm
		double norm1 = get_norm1(p);
		
		// normalize and update p and |p_temp - p|
		for(size_t ind = 0; ind < p.size(); ind++) {
			p[ind] += (1-norm1) / n;
		}


		// Compute norm to check convergence: || p_k - p_k-1 ||_\infty
		step_norm = 0;
		for(size_t ind = 0; ind < p.size(); ind++) {
			// step_norm += (p[ind] - p_old[ind]) * (p[ind] - p_old[ind]); // l2 norm
			step_norm = std::max(step_norm, abs(p[ind] - p_old[ind]));
		}
		step_norm = sqrt(step_norm);
		std::cout << "   |p_old - p| = " << step_norm << '\n';

		i += 1;

	}

	return p;
}

#endif // !PAGERANK_HPP
