
#include <unordered_set>
#include <set>

#include "../edgeList.hpp"


void kcore_decomposition(edgelist& el, std::vector<unsigned int>& ind2corevalue, std::vector<unsigned int>& ind2korder) {
	int n = el.n;

	// Set vectors to right size and 0
	ind2corevalue.resize(n);
	ind2korder.resize(n);
	std::fill(ind2corevalue.begin(), ind2corevalue.end(), 0);
	std::fill(ind2korder.begin(), ind2korder.end(), 0);

	int i = n;
	unsigned int c = 0;

	std::unordered_set<unsigned int> vertex_set;
	std::unordered_set<edge, edge_hash, edge_equal> edge_set;
	std::vector<unsigned int> vertex_degree(n, 0);

	// populating vertex set, edge set and degrees
	std::cout << "Populating vertex set, edge set and degrees...";

	for (edge e : el.edges) {
		// std::cout << "{" << e.s << "," << e.t << "}\n";
		vertex_set.insert(e.s);
		vertex_set.insert(e.t);
		edge_set.insert(e);

		vertex_degree[e.s]++;
		vertex_degree[e.t]++;
	}
	
	std::cout << " done." << '\n';

	// main iteration
	while (!vertex_set.empty()) {
		// std::cout << "\n---- Iteration" << '\n';
		// std::cout << "c:		" << c << '\n';
		// std::cout << "i:		" << i << '\n';
		std::cout << "Vertex:  	" << vertex_set.size() << '\n';
		// for (auto v : vertex_set) std::cout << v << "  ";
		// std::cout << "\nEdges:   	" << edge_set.size() << '\n';
		// for (auto e : edge_set) std::cout << "{" << e.s << "," << e.t << "},  ";
		// std::cout << '\n';
		// std::cout << "Degrees:  	" << '\n';
		// for (int i=0; i<vertex_degree.size(); i++) std::cout << i << "\t" << vertex_degree[i] << "\n";
		clock_t begin = clock();

		// Compute min degree vertex
		unsigned int v_mindeg = *vertex_set.cbegin();
		unsigned int mindeg = vertex_degree[v_mindeg];
		for (unsigned int v : vertex_set) {
			if (vertex_degree[v] < mindeg) {
				v_mindeg = v;
				mindeg = vertex_degree[v];
			}
		}

		clock_t interm = clock();


		// std::cout << "\nMinimum degree vertex: " << v_mindeg << "  with degree " << mindeg << '\n';
		
		c = std::max(c, mindeg);

		// erase v_mindeg and corresponding edges, update degrees
		vertex_set.erase(v_mindeg);
		for (auto e_it = edge_set.begin(); e_it != edge_set.end(); ++e_it) {
			std::cout << "Edge is " << "{" << (*e_it).s << "," << (*e_it).t << "},  " << '\n';
			if ((*e_it).s == v_mindeg) {
				std::cout << "/* message */ -- s" << '\n';
				edge_set.erase(e_it);
				vertex_degree[(*e_it).t]--;

				e_it++;
			}

			if ((*e_it).t == v_mindeg) {
				std::cout << "/* message */ -- t" << '\n';

				edge_set.erase(e_it);
				vertex_degree[(*e_it).s]--;

				e_it++;
			}
		}

		// Set node corevalue and core ordering index
		ind2corevalue[v_mindeg] = c;
		ind2korder[v_mindeg] = i;
		i--;

		clock_t end = clock();
		std::cout << double(interm - begin) / CLOCKS_PER_SEC << '\n';
		std::cout << double(end - interm) / CLOCKS_PER_SEC << '\n';

	}
}
