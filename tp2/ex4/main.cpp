#include <time.h>//to estimate the runing time
#include <string>
#include <vector>
#include <unordered_set>
#include <set>
#include <map>
#include <sstream>
#include <algorithm>    // std::partial_sort_copy

#include "../edgeList.hpp"
#include "kcore_edgelist.hpp"
#include "../adjArray.hpp"
#include <math.h>       /* sqrt */


int main(int argc,char** argv){
	time_t t1,t2;

	t1=time(NULL);

	double alpha = 0.15;
	double tol = 1e-8;

	if (argc==1) {
		std::cout << "Expected input: edgelist file path" << '\n';
		return 0;
	}


	// Load graph data
	std::string filePath = std::string(argv[1]);
	edgelist el = edgelist_from_txt(filePath);

	std::cout << "n = " << el.n << '\n';
	std::cout << "m = " << el.m << '\n';
	std::cout << "el length = " << el.edges.size() << '\n';

	// Running k-core algo
	std::vector<unsigned int> ind2corevalue(el.n, 0);
	std::vector<unsigned int> ind2korder(el.n, 0);
	kcore_decomposition(el, ind2corevalue, ind2korder);

	// std::cout << "ind2corevalue" << '\n';
	// for (auto kv : ind2corevalue) std::cout << kv << "  ";

	// std::cout << "\nind2korder" << '\n';
	// for (auto ko : ind2korder) std::cout << ko << "  ";
	std::vector<unsigned int>::iterator res = std::max_element(ind2corevalue.begin(), ind2corevalue.end());
	std::cout << "Maximum core value: " << *res << '\n';
	
	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}