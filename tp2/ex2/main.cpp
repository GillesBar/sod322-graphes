#include <time.h>//to estimate the runing time
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include <sstream>
#include <algorithm>    // std::partial_sort_copy

#include "../edgeList.hpp"
#include "../adjArray.hpp"
#include "../pageRank.hpp"
#include <math.h>       /* sqrt */

void write_vec(std::string filepath, std::vector<double> vec) {
	std::ofstream output_file(filepath);

	for (auto el : vec) {
		output_file << el << std::endl;
	}
	output_file.close();
}

int main(int argc,char** argv){
	time_t t1,t2;

	t1=time(NULL);

	if (argc==1) {
		std::cout << "Expected input: edgelist file path" << '\n';
		return 0;
	}

	// Load graph data
	std::string filePath = std::string(argv[1]);
	edgelist el = edgelist_from_txt(filePath);

	std::cout << "n = " << el.n << '\n';
	std::cout << "m = " << el.m << '\n';
	std::cout << "el length = " << el.edges.size() << '\n';
	

	// Computing and writing input degrees
	std::vector<double> in_degrees(el.n, 0);
	std::vector<double> out_degrees(el.n, 0);

	std::cout << "Computing in / out degrees" << '\n';
	for (edge e : el.edges) {
		out_degrees[e.s]++;
		in_degrees[e.t]++;
	}

	write_vec("in_degrees.txt", in_degrees);
	write_vec("out_degrees.txt", out_degrees);


	// Computing pageranks
	std::vector<float> alphas {0.1, 0.15, 0.2, 0.5, 0.9};
	std::map<float, std::vector<double>> alpha_to_pr;

	for(auto alpha : alphas)
	{
		std::cout << "\nComputing PageRank -- alpha = " << alpha << '\n';
		
		alpha_to_pr[alpha] = PageRank(el, alpha, 1e-8);
	}

	// 1. x = PageRank with α = 0.15, y = in-degree;
	std::ofstream output_file1("plot1.txt", std::ios::ate);

	for (size_t ind=0; ind<alpha_to_pr[0.15].size(); ind++) {
		output_file1 << alpha_to_pr[0.15][ind] << "," << in_degrees[ind] << std::endl;
	}
	output_file1.close();

	// 2. x = PageRank with α = 0.15, y = out-degree;
	std::ofstream output_file2("plot2.txt", std::ios::ate);

	for (size_t ind=0; ind<alpha_to_pr[0.15].size(); ind++) {
		output_file2 << alpha_to_pr[0.15][ind] << "," << out_degrees[ind] << std::endl;
	}
	output_file2.close();
	
	// 3. x = PageRank with α = 0.15, y = PageRank with α = 0.1;
	std::ofstream output_file3("plot3.txt", std::ios::ate);

	for (size_t ind=0; ind<alpha_to_pr[0.15].size(); ind++) {
		output_file3 << alpha_to_pr[0.15][ind] << "," << alpha_to_pr[0.1][ind] << std::endl;
	}
	output_file3.close();
	
	// 4. x = PageRank with α = 0.15, y = PageRank with α = 0.2;
	std::ofstream output_file4("plot4.txt", std::ios::ate);

	for (size_t ind=0; ind<alpha_to_pr[0.15].size(); ind++) {
		output_file4 << alpha_to_pr[0.15][ind] << "," << alpha_to_pr[0.2][ind] << std::endl;
	}
	output_file4.close();
	
	// 5. x = PageRank with α = 0.15, y = PageRank with α = 0.5;
	std::ofstream output_file5("plot5.txt", std::ios::ate);

	for (size_t ind=0; ind<alpha_to_pr[0.15].size(); ind++) {
		output_file5 << alpha_to_pr[0.15][ind] << "," << alpha_to_pr[0.5][ind] << std::endl;
	}
	output_file5.close();
	
	// 6. x = PageRank with α = 0.15, y = PageRank with α = 0.9.
	std::ofstream output_file6("plot6.txt", std::ios::ate);

	for (size_t ind=0; ind<alpha_to_pr[0.15].size(); ind++) {
		output_file6 << alpha_to_pr[0.15][ind] << "," << alpha_to_pr[0.9][ind] << std::endl;
	}
	output_file6.close();
	

	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}