#include <time.h>//to estimate the runing time
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <algorithm>    // std::partial_sort_copy

#include "../edgeList.hpp"
#include "../adjArray.hpp"
#include "../pageRank.hpp"
#include <math.h>       /* sqrt */


// Build map from vertex index to name
std::map<unsigned int, std::string> build_indtoname(std::string filepath) {
	std::map<unsigned int, std::string> ind_to_name;

    std::ifstream graphFile;
    graphFile.open(filepath, std::ifstream::in);

	// Get number of comments
	std::string line;

	int nb_comments = 0;
	getline(graphFile, line);
	while (line[0] == '#') {
		getline(graphFile, line); nb_comments++;
	}

	// Reset to file begining, jump comments, read graph
	graphFile.seekg(0, graphFile.beg);
	for (int ind = 0; ind < nb_comments; ind++) getline(graphFile, line);
	
	std::string name, str_ind;
	while (getline(graphFile, line)) {
		std::istringstream iss (line);
		std::getline(iss, str_ind, '\t');
		std::getline(iss, name, '\n');

		ind_to_name[std::stoul(str_ind)] = name;
	}
	graphFile.close();

	return ind_to_name;
}

struct Comp{
    Comp( const std::vector<double>& v, bool isinverted ) : _v(v), _invcmp(isinverted) {}
    Comp( const std::vector<double>& v ) : _v(v), _invcmp(false) {}
    bool operator ()(double a, double b) { return _invcmp ? _v[a] < _v[b] : _v[a] > _v[b]; }
    const std::vector<double>& _v;
	bool _invcmp;
};

int main(int argc,char** argv){
	time_t t1,t2;

	t1=time(NULL);

	double alpha = 0.15;
	double tol = 1e-8;

	if (argc==1) {
		std::cout << "Expected input: edgelist file path" << '\n';
		return 0;
	}

	// Load graph data
	std::string filePath = std::string(argv[1]);
	edgelist el = edgelist_from_txt(filePath);

	filePath = "../data/alr21--pageNum2Name--enwiki-20071018.txt";
	std::map<unsigned int, std::string> ind_to_name = build_indtoname(filePath);

	std::cout << "n = " << el.n << '\n';
	std::cout << "m = " << el.m << '\n';
	std::cout << "el length = " << el.edges.size() << '\n';
	
	// get proba over vertices
	std::vector<double> p = PageRank(el, alpha, tol);

	// get 5 most probable locations
	unsigned int k = 5;
	std::vector<int> indices(p.size());
	std::vector<int> p_largest_ind(k, 0);	
	std::vector<int> p_smallest_ind(k, 0);	
	for( size_t i= 0; i<p.size(); ++i ) indices[i]= i;
	partial_sort_copy( indices.begin(), indices.end(), p_largest_ind.begin(), p_largest_ind.end(), Comp(p) );
	partial_sort_copy( indices.begin(), indices.end(), p_smallest_ind.begin(), p_smallest_ind.end(), Comp(p, true) );

	std::cout << "\nLargest indices:" << '\n';
	for(size_t i = 0; i < k; i++)
	{
		unsigned int cur_ind = p_largest_ind[i];
		std::cout << p_largest_ind[i] << "\t" << p[cur_ind] << "\t" << ind_to_name[cur_ind] << '\n';
	}

	std::cout << "\nSmallest indices:" << '\n';
	for(size_t i = 0; i < k; i++)
	{
		unsigned int cur_ind = p_smallest_ind[i];
		std::cout << p_smallest_ind[i] << "\t" << p[cur_ind] << "\t" << ind_to_name[cur_ind] << '\n';
	}

	auto result = std::minmax_element (p.begin(),p.end());

	// print result:
	std::cout << "min is " << *result.first;
	std::cout << ", at position " << (result.first-p.begin()) << '\n';
	std::cout << "max is " << *result.second;
	std::cout << ", at position " << (result.second-p.begin()) << '\n';

	t2=time(NULL);

	printf("- Overall time = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));

	return 0;
}